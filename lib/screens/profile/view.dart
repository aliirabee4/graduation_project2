import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  Widget _title({String title}){
    return Container(
      width: 150,
      height: 20,
      margin: EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white
      ),
      child: Text(title),
    );
  }
  Widget _item({String title}) {
    return Container(
      margin: EdgeInsets.only(right: 20, top: 10, left: 20),
      width: MediaQuery.of(context).size.width,
      height: 50,

      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(Icons.keyboard_arrow_down),
          SizedBox(
            width: 20,
          ),
          Text(title),
        ],
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          backgroundColor: Colors.blue,
          elevation: 0,
          actions: [
            Icon(
              Icons.menu,
              color: Colors.black,
            )
          ],
          leading: Icon(
            Icons.notifications,
            color: Colors.black,
          ),
        ),
        body: ListView(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 50,
              ),
              Text(
                'حسابي ',
                style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
              )
            ],
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 150,
              height: 220,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.black45
              ),
              child: Column(children: [
                SizedBox(height: 10,),
                Container(
                  width: 80,
                  height: 80,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(80),
                    color: Colors.white
                  ),
                ),
                _title(title: 'الاسم: ايه مرعي'),
                _title(title: 'الوظيفه: مهندس '),
                _title(title: 'الرقم القومي:  2495959'),
                _title(title: 'رقم الموبايل: 20300494')
              ],),
            )
          ],),
          SizedBox(height: 20,),
          _item(title: 'رصيدي'),
          _item(title: 'سجل الاطبا'),
          _item(title: 'متابعه حالتي'),
          _item(title: 'الحجوزات المنتظره'),
          _item(title: 'الرسايل'),
          _item(title: 'اخر الاخبار'),
        ],),
      ),
    );
  }
}
