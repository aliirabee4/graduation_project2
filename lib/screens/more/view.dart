import 'package:flutter/material.dart';

class MoreScreen extends StatefulWidget {
  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          actions: [
            Icon(
              Icons.menu,
              color: Colors.black,
            )
          ],
          leading: Icon(
            Icons.notifications,
            color: Colors.black,
          ),
        ),
        body: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 50,
                ),
                Text(
                  'سجل النشاطات ',
                  style: TextStyle(color: Colors.blue, fontSize: 18, fontWeight: FontWeight.bold),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(right: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                Text('06/12/2020'),
              ],),
            ),
            Padding(
              padding: EdgeInsets.only(right: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text('Monday'),
                ],),
            ),
            Stepper(
              currentStep: 0,
                steps: [
              Step(title: Text('قمت بتقيم دكتور'), content: Text('06:30')),
              // Step(title: Text('قمت بتقيم دكتور'), content: Text('06:30')),
            ])

          ],
        ),
      ),
    );;
  }
}
