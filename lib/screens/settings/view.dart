import 'package:flutter/material.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  Widget _item({String title}) {
    return Container(
      margin: EdgeInsets.only(right: 20),
      width: MediaQuery.of(context).size.width,
      height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(Icons.home),
          SizedBox(
            width: 20,
          ),
          Text(title),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          actions: [
            Icon(
              Icons.menu,
              color: Colors.black,
            )
          ],
          leading: Icon(
            Icons.notifications,
            color: Colors.black,
          ),
        ),
        body: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 50,
                ),
                Text(
                  'الاعدادات ',
                  style: TextStyle(color: Colors.blue, fontSize: 18, fontWeight: FontWeight.bold),
                )
              ],
            ),
            _item(title: 'تعديل البيانات '),
            _item(title: 'تغيير كلمه السر '),
            _item(title: 'اداره حساباتي '),
            _item(title: 'ربط بفيس بوك '),
            _item(title: 'اعدادات الاشعارات '),
            _item(title: 'مركز المساعده '),
          ],
        ),
      ),
    );
  }
}
