import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.grey.withOpacity(0.1),
        appBar: AppBar(
          backgroundColor: Colors.white,
          actions: [
            Icon(
              Icons.menu,
              color: Colors.black,
            )
          ],
          leading: Icon(
            Icons.notifications,
            color: Colors.black,
          ),
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 15),
              child: TextFormField(
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(5),
                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                    prefixIcon: Icon(Icons.search),
                    hintText: 'بحث عن خدمه'),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 50,
                ),
                Text(
                  'الخدمات الطبيه',
                  style: TextStyle(color: Colors.blue, fontSize: 18, fontWeight: FontWeight.bold),
                )
              ],
            ),
            SizedBox(height: 10,),
            SizedBox(
              height: 140,
              child: ListView.builder(
                itemCount: 10,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (ctx, index){
                return Container(
                  width: 130,
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  height: 140,
                  decoration: BoxDecoration(
                      color: Colors.white,

                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Column(
                    children: [
                      SizedBox(height: 10,),
                      Icon(Icons.margin),
                      Text('خدمات طبيه',style: TextStyle(fontWeight: FontWeight.bold),),
                      SizedBox(height: 5,),

                      Text('بحث عن دكتور '),
                      Text('حجز عند دكتور '),
                      Text('اعاده كشف '),
                      SizedBox(height: 10,),

                      Container(
                        width: 80,
                        height: 20,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.green
                        ),
                        child: Center(child: Text('مشاهده المزيد', style: TextStyle(color: Colors.white),),),
                      )
                    ],
                  ),
                );
              }),
            ),
            SizedBox(height: 30,),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 50,
                ),
                Text(
                  'الخدمات الاكثر طلبا',
                  style: TextStyle(color: Colors.blue, fontSize: 18, fontWeight: FontWeight.bold),
                )
              ],
            ),

            SizedBox(height: 30,),
            SizedBox(
              height: 140,
              child: ListView.builder(
                  itemCount: 10,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (ctx, index){
                    return Container(
                      width: 100,
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      height: 140,
                      decoration: BoxDecoration(
                          color: Colors.white,

                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('متابعه اونلاين',style: TextStyle(fontWeight: FontWeight.bold),),
                          SizedBox(height: 10,),

                          Icon(Icons.add, size: 50,),

                        ],
                      ),
                    );
                  }),
            )
          ],
        ),
      ),
    );
    ;
  }
}
